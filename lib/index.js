const Core = require('../../core/lib/index.js')
const BaseHandler = require('./handlers/base.js')

module.exports = {
    Errors: Core.Errors,
    Handlers: {
        Base: BaseHandler,
        Body: Core.buildBodyHandler(BaseHandler),
        Json: Core.buildJsonBodyHandler(BaseHandler)
    }
}