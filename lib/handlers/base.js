const AbstractHandler = require('@functions-api/core').AbstractHandler

class BaseHandler extends AbstractHandler {
    constructor (event, context, callback) {
        super()
        this.event = event
        this.context = context
        this.callback = callback

        this.init()
    }

    static getPackageOptions() {
        return {}
    }

    setBody() {
        if (this.event.body) {
            this.body = this.event.isBase64Encoded ? Buffer.from(this.event.body, 'base64').toString() : this.event.body
        } else {
            this.body = undefined
        }
    }

    setIdentity() {
        if (this.event.requestContext.authorizer !== undefined) {
            this.identity = this.event.requestContext.authorizer.claims.client_id
        } else {
            this.identity = null
        }
    }

    setHeaders() {
        this.headers = this.event.headers || {}
    }

    setParams() {
        this.pathParams = this.event.pathParameters || {}
        this.queryParams = this.event.queryStringParameters || {}
    }
}
BaseHandler.baseType = 'lambda-proxy'

module.exports = BaseHandler