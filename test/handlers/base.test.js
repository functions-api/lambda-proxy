const BaseHandler = require('../../lib/handlers/base.js')
const Errors = require('../../../core/lib/index.js').Errors

describe("BaseHandler", () => {
    let event = {
        requestContext: {
            authorizer: {
                claims: {
                    client_id: "aclient"
                }
            }
        },
        headers: {
            "Content-Type": "text/plain"
        },
        pathParameters: {
            path: 'true'
        },
        queryStringParameters: {
            query: 'true'
        }
    }
    let context = {
        context: true
    }
    let callback = () => {}

    describe('initialization', () => {
        test('exposes the entire event', () => {
            let handler = new BaseHandler(event, context, callback)

            expect(handler.event).not.toBe(undefined)
            expect(handler.event.pathParameters.path).not.toBe(undefined)
        })

        test('exposes the context', () => {
            let handler = new BaseHandler(event, context, callback)
            expect(handler.context.context)
        })

        test('exposes the callback', () => {
            let handler = new BaseHandler(event, context, callback)
            expect(typeof(handler.callback)).toBe('function')
        })

        test('sets the cognito identity', () => {
            let handler = new BaseHandler(event, context, callback)
            expect(handler.identity).toBe('aclient')
        })

        test('sets the identity to null if not provided', () => {
            let ev = { ...event }
            delete ev.requestContext.authorizer
            let handler = new BaseHandler(ev, context, callback)
            expect(handler.identity).toBeNull()
        })

        test('sets the request headers', () => {
            let handler = new BaseHandler(event, context, callback)
            expect(handler.headers['Content-Type']).toBe('text/plain')
        })

        test('sets the path parameters', () => {
            let handler = new BaseHandler(event, context, callback)
            expect(handler.pathParams.path).toBe('true')
        })

        test('sets the query parameters', () => {
            let handler = new BaseHandler(event, context, callback)
            expect(handler.queryParams.query).toBe('true')
        })

        test('sets the  body', () => {
            let eventWithBody = Object.assign({}, event, { body: 'Hello, World!'} )
            let handler = new BaseHandler(eventWithBody, context, callback)

            expect(handler.body).toBe('Hello, World!')
        })

        test('base64 decodes the body, if the flag is set', () => {
            let eventWithBody = Object.assign({}, event, {
                body: Buffer.from('Hello, World!').toString('base64'),
                isBase64Encoded: true
            })
            let handler = new BaseHandler(eventWithBody, context, callback)

            expect(handler.body).toBe('Hello, World!')
        })
    })
})